# Welcome

This is a repository containing 6 frontend projects developed while going through Brad Traversys Modern HTML & CSS course. Details about each project are in the readme file of each corresponding folder. I choose to do this course because I was struggling with CSS and liked the syllabus of this particular course, where I could apply what I learned immediately with hands on projects. I also liked the apporach of not using frameworks yet, to build a strong foundation before starting to dive into frameworks
.

## Technologies used
Git, plain CSS and HTML and very little Javascript.
