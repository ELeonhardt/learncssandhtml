## Project context

This project is the first practical challenge from the course Modern HTML & CSS From The Beginning by Brad Traversy. It consists in building a sample Hotel Website with just pure HTML and CSS. A particularity is that as this point in the course, Flexbox and CSS grid have not been introduced yet, which is why it has not been used to create the pages.
As an addition, there are media queries added to make the page responsive. Server-side functionalities like sending emails are not working at the moment, since it was out of the scope for the course.

## Motivation

Building another project, I found myself struggling with CSS and decided to take the course to build a stronger foundation. Instead of following along with the instructor to build this site, I tried to first implement the pages myself and then corrected some aspects of my implementation while watching the follow-along video, which is why the css turned out a little messy and the naming might appear incoherent. Since my objective with this project was learning the basics of CSS, I have not corrected theses aspects, although I might come back to it in the future to make it prettier.